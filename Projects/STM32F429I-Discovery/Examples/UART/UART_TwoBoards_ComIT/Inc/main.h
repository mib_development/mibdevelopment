/**
  ******************************************************************************
  * @file    UART/UART_TwoBoards_ComIT/Inc/main.h 
  * @author  MCD Application Team
  * @version V1.2.0
  * @date    26-December-2014
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f429i_discovery.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* User can use this section to tailor USARTx/UARTx instance used and associated 
   resources */
/* Definition for USARTx clock resources */
#define USARTx                           USART1
#define USARTx_CLK_ENABLE()              __HAL_RCC_USART1_CLK_ENABLE();
#define DMAx_CLK_ENABLE()                __HAL_RCC_DMA2_CLK_ENABLE()
#define USARTx_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()
#define USARTx_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE() 

#define USARTx_FORCE_RESET()             __HAL_RCC_USART1_FORCE_RESET()
#define USARTx_RELEASE_RESET()           __HAL_RCC_USART1_RELEASE_RESET()

/* Definition for USARTx Pins */
#define USARTx_TX_PIN                    GPIO_PIN_9
#define USARTx_TX_GPIO_PORT              GPIOA  
#define USARTx_TX_AF                     GPIO_AF7_USART1
#define USARTx_RX_PIN                    GPIO_PIN_10
#define USARTx_RX_GPIO_PORT              GPIOA 
#define USARTx_RX_AF                     GPIO_AF7_USART1


/* Definition for USARTx's DMA */
#define USARTx_TX_DMA_CHANNEL            DMA_CHANNEL_4
#define USARTx_TX_DMA_STREAM             DMA2_Stream7
#define USARTx_RX_DMA_CHANNEL            DMA_CHANNEL_4
#define USARTx_RX_DMA_STREAM             DMA2_Stream5


/* Definition for USARTx's NVIC */
#define USARTx_IRQn                      USART1_IRQn
#define USARTx_IRQHandler                USART1_IRQHandler
#define USARTx_DMA_TX_IRQn               DMA2_Stream7_IRQn
#define USARTx_DMA_RX_IRQn               DMA2_Stream5_IRQn
#define USARTx_DMA_TX_IRQHandler         DMA2_Stream7_IRQHandler
#define USARTx_DMA_RX_IRQHandler         DMA2_Stream5_IRQHandler

/* Size of Transmission buffer */
#define TXBUFFERSIZE                      (COUNTOF(aTxBuffer) - 1)
/* Size of Reception buffer */
#define RXBUFFERSIZE                      TXBUFFERSIZE




/*
	 ** Initialize the uart Port interfacing with MP2128G2 
	 */
#define USARTy                           USART2
#define USARTy_CLK_ENABLE()              __HAL_RCC_USART2_CLK_ENABLE();
#define DMAy_CLK_ENABLE()                __HAL_RCC_DMA1_CLK_ENABLE()
#define USARTy_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOD_CLK_ENABLE()
#define USARTy_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOD_CLK_ENABLE() 

#define USARTy_FORCE_RESET()             __HAL_RCC_USART2_FORCE_RESET()
#define USARTy_RELEASE_RESET()           __HAL_RCC_USART2_RELEASE_RESET()

/* Definition for USARTy Pins */
#define USARTy_TX_PIN                    GPIO_PIN_5
#define USARTy_TX_GPIO_PORT              GPIOD  
#define USARTy_TX_AF                     GPIO_AF7_USART2
#define USARTy_RX_PIN                    GPIO_PIN_6
#define USARTy_RX_GPIO_PORT              GPIOD 
#define USARTy_RX_AF                     GPIO_AF7_USART2

/* Definition for USARTy's DMA */
#define USARTy_TX_DMA_CHANNEL            DMA_CHANNEL_4
#define USARTy_TX_DMA_STREAM             DMA1_Stream6
#define USARTy_RX_DMA_CHANNEL            DMA_CHANNEL_4
#define USARTy_RX_DMA_STREAM             DMA1_Stream5

/* Definition for USARTy's NVIC */
#define USARTy_IRQn                      USART2_IRQn
#define USARTy_IRQHandler                USART2_IRQHandler
#define USARTy_DMA_TX_IRQn               DMA1_Stream6_IRQn
#define USARTy_DMA_RX_IRQn               DMA1_Stream5_IRQn
#define USARTy_DMA_TX_IRQHandler         DMA1_Stream6_IRQHandler
#define USARTy_DMA_RX_IRQHandler         DMA1_Stream5_IRQHandler

/* Size of Transmission buffer */
#define TXBUFFERSIZE                     (COUNTOF(aTxBuffer) - 1)
/* Size of Reception buffer */
#define RXBUFFERSIZE                     TXBUFFERSIZE



/*
	 ** Initialize the uart Port interfacing with MP2128G2 
	 */
#define USARTz                           USART3
#define USARTz_CLK_ENABLE()              __HAL_RCC_USART3_CLK_ENABLE();
#define DMAz_CLK_ENABLE()                __HAL_RCC_DMA1_CLK_ENABLE()
#define USARTz_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOC_CLK_ENABLE()
#define USARTz_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOC_CLK_ENABLE() 

#define USARTz_FORCE_RESET()             __HAL_RCC_USART3_FORCE_RESET()
#define USARTz_RELEASE_RESET()           __HAL_RCC_USART3_RELEASE_RESET()

/* Definition for USARTz Pins */
#define USARTz_TX_PIN                    GPIO_PIN_10
#define USARTz_TX_GPIO_PORT              GPIOC  
#define USARTz_TX_AF                     GPIO_AF7_USART3
#define USARTz_RX_PIN                    GPIO_PIN_11
#define USARTz_RX_GPIO_PORT              GPIOC 
#define USARTz_RX_AF                     GPIO_AF7_USART3

/* Definition for USARTz's DMA */
#define USARTz_TX_DMA_CHANNEL            DMA_CHANNEL_4
#define USARTz_TX_DMA_STREAM             DMA1_Stream3
#define USARTz_RX_DMA_CHANNEL            DMA_CHANNEL_4
#define USARTz_RX_DMA_STREAM             DMA1_Stream1

/* Definition for USARTz's NVIC */
#define USARTz_IRQn                      USART3_IRQn
#define USARTz_IRQHandler                USART3_IRQHandler
#define USARTz_DMA_TX_IRQn               DMA1_Stream3_IRQn
#define USARTz_DMA_RX_IRQn               DMA1_Stream1_IRQn
#define USARTz_DMA_TX_IRQHandler         DMA1_Stream3_IRQHandler
#define USARTz_DMA_RX_IRQHandler         DMA1_Stream1_IRQHandler

/* Size of Transmission buffer */
#define TXBUFFERSIZE                     (COUNTOF(aTxBuffer) - 1)
/* Size of Reception buffer */
#define RXBUFFERSIZE                     TXBUFFERSIZE



#define CHECKSUM_SIZE_IN_BYTES						2
#define HEADER_SIZE_IN_BYTES							7
#define PACKET_ID_INDEX										5
#define PAYLOAD_LENGTH_INDEX							3
#define PAYLOAD_START_INDEX								7
#define PACKET_SIZE_IN_BYTES 				(HEADER_SIZE_IN_BYTES+CHECKSUM_SIZE_IN_BYTES+aRxBufferGCS[PAYLOAD_LENGTH_INDEX])
#define PACKET_SIZE_IN_BYTES_MTN 			 getIntFromByte( transmitBuffer+PAYLOAD_LENGTH_INDEX) + CHECKSUM_SIZE_IN_BYTES+HEADER_SIZE_IN_BYTES
#define PACKET_SIZE_IN_BYTES_MP 				 getIntFromByte( aRxBuffer+PAYLOAD_LENGTH_INDEX) + CHECKSUM_SIZE_IN_BYTES+HEADER_SIZE_IN_BYTES

/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))
	



/* Exported functions ------------------------------------------------------- */
/*Initialization Of the UART*/
HAL_StatusTypeDef initUartChannel(UART_HandleTypeDef * uart, USART_TypeDef * usartInstance,uint32_t baudRate,
																	uint32_t wordLength,uint32_t stopBits,uint32_t parity,uint32_t HwFlowCtl,uint32_t mode,
																	uint32_t OverSampling);



#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
