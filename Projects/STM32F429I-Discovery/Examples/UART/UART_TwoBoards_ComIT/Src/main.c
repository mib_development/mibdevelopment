/**
  ******************************************************************************
  * @file    UART/UART_TwoBoards_ComIT/Src/main.c 
  * @author  MCD Application Team
  * @version V1.2.0
  * @date    26-December-2014
  * @brief   This sample code shows how to use STM32F4xx UART HAL API to transmit 
  *          and receive a data buffer with a communication process based on
  *          IT transfer. 
  *          The communication is done using 2 Boards.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <string.h>

/** @addtogroup STM32F4xx_HAL_Examples
  * @{
  */

/** @addtogroup UART_TwoBoards_ComIT
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define TRANSMITTER_BOARD

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
__IO ITStatus UartReady = RESET;
__IO FlagStatus portionToBeReceived = RESET;
__IO FlagStatus packetReceived = RESET; 
__IO FlagStatus portionToBeReceived1 = RESET;
__IO FlagStatus packetReceived1 = RESET; 
__IO FlagStatus portionToBeReceived2 = RESET;
__IO FlagStatus packetReceived2 = RESET; 

int startnavigationflag = 0;
int ToMP_ = 0;
int firstinvocationflag = 1;

//__IO FlagStatus packetReceived1 = RESET; 
/* Buffer used for transmission */
uint8_t aTxBuffer[] = " UART TO Be Communicated untill finish ";
uint8_t aRxBuffer[700];

/* Buffer used for reception */
uint8_t aRxBufferGCS[100];
uint8_t aRxBufferMTN[700];
uint8_t aRxBufferMTN_[700];
uint8_t * transmitBuffer ; 
uint8_t * receiveBuffer ; 
uint8_t firsttimeflag = 1;

/* UART handler declaration */
UART_HandleTypeDef UartMTNHandle,UartGCSHandle , UartMPHandle;

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void Error_Handler(void);
static uint16_t Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
	
	uint16_t getIntFromByte(uint8_t * byteArray){
	
	uint16_t SixtyByte = (uint16_t) (*byteArray  & 0x00FF);
	SixtyByte = (uint16_t) ((*(byteArray+1)<<8 & 0xFF00) |SixtyByte) ;
	return SixtyByte;
	
	}
	
	uint16_t sizeInBytes ; 
int main(void)
{

  /* STM32F4xx HAL library initialization:
       - Configure the Flash prefetch, instruction and Data caches
       - Configure the Systick to generate an interrupt each 1 msec
       - Set NVIC Group Priority to 4
       - Global MSP (MCU Support Package) initialization
     */
  HAL_Init();
  
  /* Configure LED3 and LED4 */
  BSP_LED_Init(LED3);
  BSP_LED_Init(LED4);

  /* Configure the system clock to 180 MHz */
  SystemClock_Config();
  

	
	/*intialization of GCS and MTN and MP uart handlers*/
	

			if(initUartChannel(&UartMTNHandle,USARTx,3000000,UART_WORDLENGTH_8B,UART_STOPBITS_1,UART_PARITY_NONE,UART_HWCONTROL_NONE
		,UART_MODE_TX_RX,UART_OVERSAMPLING_16) != HAL_OK){
    Error_Handler();
  }
     if(initUartChannel(&UartGCSHandle,
			 USARTy,115200,UART_WORDLENGTH_8B,UART_STOPBITS_1,UART_PARITY_NONE,UART_HWCONTROL_NONE
		,UART_MODE_TX_RX,UART_OVERSAMPLING_16) != HAL_OK){
    Error_Handler();
  }
		
		if(initUartChannel(&UartMPHandle,USARTz,9600,UART_WORDLENGTH_8B,UART_STOPBITS_1,UART_PARITY_NONE,UART_HWCONTROL_NONE
		,UART_MODE_TX_RX,UART_OVERSAMPLING_16) != HAL_OK){
    Error_Handler();
  }
  

  /* Configure USER Button */
  BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);
  /* Wait for USER Button press before starting the Communication */
  while (BSP_PB_GetState(BUTTON_KEY) == RESET)
  {
  }
  
  /* The board sends the message and expects to receive it back */
  /*##-2- Start the transmission process #####################################*/  
  /* While the UART in reception process, user can transmit data through 
     "aTxBuffer" buffer */
  if(HAL_UART_Transmit_IT(&UartGCSHandle, (uint8_t*)aTxBuffer, TXBUFFERSIZE)!= HAL_OK)
  {
    Error_Handler();
  }
  
  /*##-3- Wait for the end of the transfer ###################################*/   
  while (UartReady != SET)
  {
  }
  
  /* Reset transmission flag */
  UartReady = RESET;
	
	receiveBuffer = aRxBufferMTN; 
	transmitBuffer = aRxBufferMTN_;
  
  /* Turn LED3 Off */
  BSP_LED_Off(LED3);
  
  /*##-4- Put UART peripheral in reception process ###########################*/  
  if(HAL_UART_Receive_IT(&UartGCSHandle, (uint8_t *)aRxBufferGCS, HEADER_SIZE_IN_BYTES) != HAL_OK)
  {
    Error_Handler();
  }
	if(HAL_UART_Receive_IT(&UartMTNHandle, (uint8_t *)receiveBuffer, HEADER_SIZE_IN_BYTES) != HAL_OK)
  {
    Error_Handler();
  }
	

  UartReady = RESET;
	
	

  /* Infinite loop */
  while (1)
  {
		
		/*if MIB receieve any command from GCS exept START NAVIGATION transmit it to MTN using interrupts 
		and if it is START NAVIGATION transmit it to MTN too but raise start navigation flag*/
		if(packetReceived == SET){
			packetReceived = RESET;
			if (aRxBufferGCS[PACKET_ID_INDEX]==0xE && aRxBufferGCS[PACKET_ID_INDEX+1] ==0xE )
			{	
				BSP_LED_On(LED4);
				ToMP_ = 1 ; 			
			}
			
			else
			{
				HAL_UART_Transmit_IT(&UartMTNHandle, (uint8_t*)aRxBufferGCS, PACKET_SIZE_IN_BYTES);
			}
	
			
		}
		
		
		
		uint8_t i = 0 ; 
		uint8_t ii = 1 ;
		uint8_t iii = 2 ;
		/*if MIB receives reponse from MTN transmit it back to GCS*/
		if(packetReceived1 == SET)
		{
			packetReceived1 = RESET;
			
				if(ToMP_ == 0){
			
			HAL_UART_Transmit_IT(&UartGCSHandle, transmitBuffer, PACKET_SIZE_IN_BYTES_MTN);	
				}
			/*Debugging Only*/
			/*while (UartReady ==RESET);
			if (transmitBuffer == aRxBufferMTN){
				HAL_UART_Transmit_IT(&UartGCSHandle, (uint8_t *)&i, 1);
			} else 	if (transmitBuffer == aRxBufferMTN_){
				HAL_UART_Transmit_IT(&UartGCSHandle, (uint8_t *)&ii, 1);
			} else {
			HAL_UART_Transmit_IT(&UartGCSHandle, (uint8_t *)&iii, 1);
			}*/
			//while (UartReady == RESET); 
			
		
		
		
		/*if MIB receives start navigation response from MTN transmit it back to MP*/
		/*it transmit it now to GCS for testing purpose*/
		else if(ToMP_ == 1)
		{
			
			/*******************************************************************************************************************************/
			/*generating micro pilot packet*/
			/*******************************************************************************************************************************/
			
			
			//defining arrays holding MP data and packet
			uint8_t MP_Packet [39];
			uint8_t MP_Packet_SOF[2] = {'M','R'};
			uint8_t ReplaceGPSPosE [4];
			uint8_t ReplaceGPSPosN [4];
			uint8_t ReplaceGPSPosU [4];
			uint8_t ReplaceGPSVeIE [4];
			uint8_t ReplaceGPSVeIN [4];
			uint8_t ReplaceGPSVeIU [4];
			uint8_t Pitch[4];
			uint8_t Roll[4];
			uint8_t Yaw[4];
			uint8_t MP_Packet_EOF[1] = {';'};
			
			//extract required data from Input data sent by MTN
			
			double Double_ReplaceGPSPosE;
      memcpy(&Double_ReplaceGPSPosE, transmitBuffer+88, sizeof(double));
			Double_ReplaceGPSPosE = (Double_ReplaceGPSPosE)*(3.14/180.0)*(500000000); 
			long Long_ReplaceGPSPosE = (long) Double_ReplaceGPSPosE;
			memcpy(ReplaceGPSPosE, (uint8_t*) (&Long_ReplaceGPSPosE), 4);
			
			double Double_ReplaceGPSPosN;
      memcpy(&Double_ReplaceGPSPosN, transmitBuffer+96, sizeof(double));
			Double_ReplaceGPSPosN = (Double_ReplaceGPSPosN)*(3.14/180.0)*(500000000); 
			long Long_ReplaceGPSPosN = (long) Double_ReplaceGPSPosN;
			memcpy(ReplaceGPSPosN, (uint8_t*) (&Long_ReplaceGPSPosN), 4);
			
			double Double_ReplaceGPSPosU;
      memcpy(&Double_ReplaceGPSPosU, transmitBuffer+104, sizeof(double));
			Double_ReplaceGPSPosU = (Double_ReplaceGPSPosU)*(3.28084)*(-8); 
			long Long_ReplaceGPSPosU = (long) Double_ReplaceGPSPosU;
			memcpy(ReplaceGPSPosU, (uint8_t*) (&Long_ReplaceGPSPosU), 4);
			
			
			float Float_ReplaceGPSVeIE;
      memcpy(&Float_ReplaceGPSVeIE, transmitBuffer+124, sizeof(float));
			Float_ReplaceGPSVeIE = (Float_ReplaceGPSVeIE)*(3.28084); 
			long Long_ReplaceGPSVeIE = (long) Float_ReplaceGPSVeIE;
			memcpy(ReplaceGPSVeIE, (uint8_t*) (&Long_ReplaceGPSVeIE), 4);
			
			
			float Float_ReplaceGPSVeIN;
      memcpy(&Float_ReplaceGPSVeIN, transmitBuffer+128, sizeof(float));
			Float_ReplaceGPSVeIN = (Float_ReplaceGPSVeIN)*(3.28084); 
			long Long_ReplaceGPSVeIN = (long) Float_ReplaceGPSVeIN;
			memcpy(ReplaceGPSVeIN, (uint8_t*) (&Long_ReplaceGPSVeIN), 4);
			
			
			float Float_ReplaceGPSVeIU;
      memcpy(&Float_ReplaceGPSVeIU, transmitBuffer+132, sizeof(float));
			Float_ReplaceGPSVeIU = (Float_ReplaceGPSVeIU)*(3.28084); 
			long Long_ReplaceGPSVeIU = (long) Float_ReplaceGPSVeIU;
			memcpy(ReplaceGPSVeIU, (uint8_t*) (&Long_ReplaceGPSVeIU), 4);
			
			
			
			double Double_Pitch;
      memcpy(&Double_Pitch, transmitBuffer+24, sizeof(double));
			Double_Pitch = (Double_Pitch)*(0.0174532925)*(1024); 
			long Long_Pitch = (long) Double_Pitch;
			memcpy(Pitch, (uint8_t*) (&Long_Pitch), 4);
			
			double Double_Roll;
      memcpy(&Double_Roll, transmitBuffer+16, sizeof(double));
			Double_Roll = (Double_Roll)*(0.0174532925)*(1024); 
			long Long_Roll = (long) Double_Roll;
			memcpy(Roll, (uint8_t*) (&Long_Roll), 4);
			
			
			double Double_Yaw;
      memcpy(&Double_Yaw, transmitBuffer+32, sizeof(double));
			Double_Yaw = (Double_Yaw)*(0.0174532925)*(1024); 
			long Long_Yaw = (long) Double_Yaw;
			memcpy(Yaw, (uint8_t*) (&Long_Yaw), 4);
			
			
			//generating MP packet 
			int counter;
			
			MP_Packet[0] = MP_Packet_SOF[0];
			MP_Packet[1] = MP_Packet_SOF[1];
			
			for(counter = 2 ; counter < 6; counter++)
			{
				MP_Packet[counter] = ReplaceGPSPosE[counter - 2];
			}
			
			for(counter = 6 ; counter < 10; counter++)
			{
				MP_Packet[counter] = ReplaceGPSPosN[counter - 6];
			}
			
			for(counter = 10 ; counter < 14; counter++)
			{
				MP_Packet[counter] = ReplaceGPSPosU[counter - 10];
			}
			
			for(counter = 14 ; counter < 18; counter++)
			{
				MP_Packet[counter] = ReplaceGPSVeIE[counter - 14];
			}
			
			for(counter = 18 ; counter < 22; counter++)
			{
				MP_Packet[counter] = ReplaceGPSVeIN[counter - 18];
			}
			
			
			for(counter = 22 ; counter < 26; counter++)
			{
				MP_Packet[counter] = ReplaceGPSVeIU[counter - 22];
			}
			
			
			for(counter = 26 ; counter < 30; counter++)
			{
				MP_Packet[counter] = Pitch[counter - 26];
			}
			
			
			for(counter = 30 ; counter < 34; counter++)
			{
				MP_Packet[counter] = Roll[counter - 30];
			}
			
			
			for(counter = 34 ; counter < 38; counter++)
			{
				MP_Packet[counter] = Yaw[counter - 34];
			}
			
			MP_Packet[38] = MP_Packet_EOF[0];
			
			
			
			/*******************************************************************************************************************************/
			/*******************************************************************************************************************************/
			//replace arxbuffer with MP_Packet and size with 39
		//HAL_UART_Transmit_IT(&UartGCSHandle, transmitBuffer, PACKET_SIZE_IN_BYTES_MTN);	
		HAL_UART_Transmit_IT(&UartMPHandle, MP_Packet,39);	
		
		}
		
    BSP_LED_Toggle(LED3);
		
  }
	
	}
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 180000000
  *            HCLK(Hz)                       = 180000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 8
  *            PLL_N                          = 360
  *            PLL_P                          = 2
  *            PLL_Q                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 5
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 360;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /* Activate the Over-Drive mode */
  HAL_PWREx_EnableOverDrive();
 
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;  
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief  Tx Transfer completed callback
  * @param  UartHandle: UART handle. 
  * @note   This example shows a simple way to report end of IT Tx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  /* Set transmission flag: transfer complete*/
  UartReady = SET;

  /* Turn LED3 on: Transfer in transmission process is correct */
  BSP_LED_On(LED3);
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of IT Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallbackDMA(UART_HandleTypeDef *UartHandle)
{
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{

  /* Turn LED3 on: Transfer in reception process is correct */
  BSP_LED_Toggle(LED3);

	/*first check who is the caller of call back function GCS or MTN*/
	
	if(UartHandle == &UartGCSHandle)
	{
		/*if the caller is GCS check if h received header then receive the remaining of packet 
		otherwise receive new header and raise flag that MIB received full packet*/
	if (portionToBeReceived == RESET){
		portionToBeReceived =SET;
		
		if(HAL_UART_Receive_IT(UartHandle, (uint8_t *)(aRxBufferGCS+PAYLOAD_START_INDEX), aRxBufferGCS[PAYLOAD_LENGTH_INDEX] + CHECKSUM_SIZE_IN_BYTES) != HAL_OK)
		{
			Error_Handler();
		}
	}
	
	
	else {
		portionToBeReceived =RESET;
		packetReceived = SET; 
		
		if(HAL_UART_Receive_IT(UartHandle, (uint8_t *)aRxBufferGCS, HEADER_SIZE_IN_BYTES) != HAL_OK)
		{
			Error_Handler();
		}
		
	}
	
}

	
else if(UartHandle == &UartMTNHandle)
{

		/*if the caller is MTN check first are we in start navigation state or non start navigation state then check if received header then receive the remaining of packet 
		otherwise receive new header and raise flag that MIB received full packet*/	
	
/*if(startnavigationflag == 1)
{
	  if (portionToBeReceived2 == RESET){
			portionToBeReceived2 = SET;
			uint16_t ii = getIntFromByte( receiveBuffer+PAYLOAD_LENGTH_INDEX); 
		
			if(HAL_UART_Receive_IT(UartHandle, (uint8_t *)(receiveBuffer+PAYLOAD_START_INDEX),
				ii + CHECKSUM_SIZE_IN_BYTES) != HAL_OK)
				{
					Error_Handler();
				}
		
		}
		
		else {
		
			portionToBeReceived2 =RESET;
		packetReceived2 = SET;
		
	
		if (UartReady == SET){
		uint8_t * temp = transmitBuffer ; 
			transmitBuffer = receiveBuffer; 
			receiveBuffer = temp ;
			UartReady = RESET ;
		}
		
	if(HAL_UART_Receive_IT(&UartMTNHandle, (uint8_t *)receiveBuffer, HEADER_SIZE_IN_BYTES) != HAL_OK)
		{
			Error_Handler();
		}
			
		}

}
	
	
else{	
			
	*/
			
if (portionToBeReceived1 == RESET){
		portionToBeReceived1 =SET;
		uint16_t ii = getIntFromByte( receiveBuffer+PAYLOAD_LENGTH_INDEX); 
	if(HAL_UART_Receive_IT(UartHandle, (uint8_t *)(receiveBuffer+PAYLOAD_START_INDEX),
		ii + CHECKSUM_SIZE_IN_BYTES) != HAL_OK)
		{
			Error_Handler();
		}
		
	}
	
	
	else {
		
		portionToBeReceived1 =RESET;
		packetReceived1 = SET;
		
		if (UartReady == SET){
		uint8_t * temp = transmitBuffer ; 
			transmitBuffer = receiveBuffer; 
			receiveBuffer = temp ;
			UartReady = RESET ;
		}
		if(HAL_UART_Receive_IT(UartHandle, (uint8_t *)receiveBuffer, HEADER_SIZE_IN_BYTES) != HAL_OK)
			{
				Error_Handler();
			}
		
	}
}	
	
//}


	
else
{
}

}




/**
  * @brief  UART error callbacks
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
 void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle)
{
  /* Turn LED3 on: Transfer error in reception/transmission process */
  BSP_LED_On(LED3); 
}

/**
  * @brief  Compares two buffers.
  * @param  pBuffer1, pBuffer2: buffers to be compared.
  * @param  BufferLength: buffer's length
  * @retval 0  : pBuffer1 identical to pBuffer2
  *         >0 : pBuffer1 differs from pBuffer2
  */
static uint16_t Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength)
{
  while (BufferLength--)
  {
    if ((*pBuffer1) != *pBuffer2)
    {
      return BufferLength;
    }
    pBuffer1++;
    pBuffer2++;
  }

  return 0;
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  /* Turn LED4 on */
  BSP_LED_On(LED4);
  while(1)
  {
  }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
